# siteHX
A simple static site generator.

This repository hosts the source of version 2.0.0 onwards, sources for older versions can be downloaded [here](https://lib.haxe.org/p/siteHX).

## Building
```bash
$ haxelib install build.hxml
$ haxe build.hxml
```

## Usage
There are no complex commands. Simply launch the executable in a directory where you Markdown files are located (along with the template and the config) and you're ready to go!

Or, point siteHX to a different site directory like the following: `./sitehx /path/to/site`

siteHX can be also installed through Haxelib (`haxelib install sitehx`) and can be used from there! (`haxelib run sitehx /path/to/site`)

## Templating guide
siteHX uses a different templating system and a slightly limited set of fields.

### List of fields
* `::name::` - your website name from `config.yml`
* `::description::` - your website description from `config.yml`
* `::author::` - your name from `config.yml`
* `::content::` - content from a Markdown file

### Includes
Includes can be placed under `_includes` directory and are automatically detected by siteHX. They can be added by simply typing `::include_<Template name>::` into your `template.html` file, which must be placed on the root directory of your website.

## Examples

### template.html
```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>::title::</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="generator" content="siteHX">
    </head>
    <body>
        ::content::
        ::include_test::
    </body>
</html>
```

### config.yml
```yaml
name: Website name
description: Website description
author: Your name
cleanLinks: false
```

The `cleanLinks` field enables a feature to place your compiled website pages into their respective folders rather than being located on the root directory.