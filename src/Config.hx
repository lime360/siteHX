package;

import yaml.Yaml;
import yaml.Parser;
import yaml.util.ObjectMap;

typedef SiteConfig = {
    name:String,
    description:String,
    author:String,
    cleanLinks:String
}

class Config
{
    public static function load(yamlPath:String):SiteConfig {
        var data:ObjectMap<Dynamic, String> = Yaml.read(yamlPath);

        var config:SiteConfig = {
            name: data.get("name"),
            description: data.get("description"),
            author: data.get("author"),
            cleanLinks: data.get("cleanLinks")
        }

        return config;
    }
}