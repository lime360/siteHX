package;

import haxe.io.Path;

class AppPath
{
    public static function getPath() {
        #if (haxe >= "4.3.2")
        return Path.directory(Sys.programPath());
        #else
        return Path.directory(Sys.executablePath());
        #end
    }
}