package;

import haxe.io.Path;
import haxe.Template;
import sys.io.File;
import sys.FileSystem;
import Reflect;
import AppPath;
import Config;

class Main
{
	public static function listPages(dir:String)
	{
		var pages:Array<String> = [];

		if (FileSystem.exists(dir))
		{
			for (file in FileSystem.readDirectory(dir))
			{
				var path = Path.join([dir, file]);

				if (!FileSystem.isDirectory(path) && Path.extension(path) == "md")
				{
					pages.push(path);
				}
			}
			return pages;
		} else {
			return [];
		}
	}

	public static function listIncludes(dir:String)
	{
		var includes:Map<String, String> = new Map<String, String>();
		var incDir = Path.join([dir, "_includes"]);

		if (FileSystem.exists(incDir))
		{
			for (file in FileSystem.readDirectory(incDir))
			{
				var path = Path.join([incDir, file]);

				if (!FileSystem.isDirectory(path) && Path.extension(path) == "html") 
				{
					var name = Path.withoutExtension(file);
					includes.set("include_" + name, File.getContent(path));
				}
			}
			return includes;
		} else {
			return [];
		}
	}

	public static function buildSite()
	{
		var source:String;

		if (Sys.args().length > 0)
		{
			source = Sys.args()[0];
		} else {
			source = AppPath.getPath();
		}
		
		var output = Path.join([source, "_site"]);

		if (!FileSystem.exists(output))
		{
			FileSystem.createDirectory(output);
		}

		var config = Config.load(Path.join([source, "config.yml"]));

		var includes = listIncludes(source);

		for (file in listPages(source))
		{
			var input = File.getContent(file);
			var templateContent = File.getContent(source + "/template.html");
			var trimmed:String = Path.withoutDirectory(file);
			var outputFile:String;

			if (config.cleanLinks == "true")
			{
				if (!FileSystem.exists(Path.join([output, trimmed.substring(0, trimmed.length - ".md".length)])))
				{
					FileSystem.createDirectory(Path.join([output, trimmed.substring(0, trimmed.length - ".md".length)]));
				}

				outputFile = Path.join([source, "_site", trimmed.substring(0, trimmed.length - ".md".length) + "/index.html"]);
			} else {
				outputFile = Path.join([source, "_site", trimmed.substring(0, trimmed.length - ".md".length) + ".html"]);
			}

			var content:String = Markdown.markdownToHtml(input);
			var tpldata = {
				content: content,
				title: config.name != null ? config.name : trimmed,
				description: config.description != null ? config.description : "",
				author: config.author != null ? config.author : "",
			}

			for (incName in includes.keys())
			{
				Reflect.setField(tpldata, incName, includes.get(incName));
			}

			if (content != null && content != "")
			{
				var template:Template = new Template(templateContent);
				File.saveContent(outputFile, template.execute(tpldata));
				Sys.println(file + " => " + outputFile);
			} else {
				Sys.println(file + " has been skipped (no contents found)");
			}
		}

		Sys.println("Done!\n" + Std.string(listPages(source).length) + " pages have been processed");
	}

	public static function main()
	{
		buildSite();
		Sys.exit(0);
	}
}